import React from 'react';

class AddDevice extends React.Component {
  
    state = {name: '',price:""};
    
    addDevice() {
        
     this.props.onAdd({
       name:this.name,
       price:this.price
   });
  }
  
    handleNameChange(event) {
    this.setState({name: event.target.value});
  }
    handlePriceChange(event) {
    this.setState({price: event.target.value});
  }
  
  
    render() {
        return (
            <div>
                <input type="text" name="name" id="name"/>
                <input type="text" name="price" id="price" />
                <button onClick={this.addDevice} > </button>
            </div>
        )
    }
    
    
}

export default AddDevice;