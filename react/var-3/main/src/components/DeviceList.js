import React from 'react';

import AddDevice from './AddDevice';
class DeviceList extends React.Component {
    constructor(){
        super();
        this.state = {
            devices: []
        };
        this.callbackFunction = this.callbackFunction.bind(this);
    }   
    callbackFunction = (data) => {
     const devices = this.state.devices.slice();
     devices.push(data);
     
      this.setState(devices)
}
  
    render(){
        return (
            
            <div>
            <AddDevice onAdd = {this.callbackFunction}/>
            
            </div>
        )
    }
}

export default DeviceList;