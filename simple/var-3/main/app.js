const express = require('express');
const cors = require('cors');
const path = require('path');

const app = express();

app.use(cors());
app.get ("/data", (req,res) => res.send(require("./data.json")))
app.use(express.static('public'))

module.exports = app;